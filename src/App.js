
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Locator, { Resolve } from 'locustjs-locator';
import { PeriodServiceBase, PeriodServiceFake } from './components/Services/PeriodService';
import { SckOrdLsServiceBase, SckOrdLsServiceFake } from './components/Services/SckOrdLsService';
import './App.scss';
import Content from './router'

Locator.Instance.register(PeriodServiceBase, PeriodServiceFake);
Locator.Instance.register(SckOrdLsServiceBase,SckOrdLsServiceFake);

function App() {

  return (
    <BrowserRouter  >
      <div>
        <Content />
      </div>
    </BrowserRouter >
  );
}

export default App;