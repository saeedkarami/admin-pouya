import PeriodServiceBase from './PeriodServiceBase';

class PeriodServiceFake extends PeriodServiceBase {
  getAll() {
    const _this = this;

    return new Promise((res, rej) => {
      const periods = [
        { Number: 1, Year: 1396 },
        { Number: 2, Year: 1397 },
        { Number: 3, Year: 1398 },
        { Number: 4, Year: 1399 }
      ];
      let data = localStorage.getItem('periods');
      if (data) {
        try {
          data = JSON.parse(data);
        } catch (error) {

        }
      }

      if (Array.isArray(data)) {
        res(data)
      } else {
        _this.save(periods);

        res(periods);
      }
    });
  }
  save(data) {
    localStorage.setItem('periods', JSON.stringify(data));
  }

  async add(period) {
    if (!period) {
      period = {}
    }

    let data = await this.getAll();
    period.Number = period.Number ? period.Number : (data.reduce((acc, cur) => Math.max(acc, cur.Number), 0) + 1);
    period.Year = period.Year ? period.Year : (data.reduce((acc, cur) => Math.max(acc, cur.Year), 0) + 1);
    data.push(period);
    console.log(data)
    this.save(data);
    return data;
  }

  async deleteByNumber(period) {
    if (!period) {
      period = []
    }
    let data = await this.getAll();
    data.pop(period)
    this.save(data);
    return data;
  }

  async deleteByYear(year) {
    if (!year) {
      throw 'Please specify which year you want to delete'
    }
    
    let data = await this.getAll();

    data = data.filter(x => x.Year != year);

    this.save(data);

    return data;
  }
}

export default PeriodServiceFake;