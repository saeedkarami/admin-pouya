import PeriodServiceBase from './PeriodServiceBase';

class PeriodServiceByFetch extends PeriodServiceBase {
  async getAll() {
    let res = await fetch('/data/periods.json');
    let data = await res.json();

    return data;
  }
}
export default PeriodServiceByFetch;
