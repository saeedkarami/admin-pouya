
export default class PeriodServiceBase {
    constructor() {
        if (this.constructor === PeriodServiceBase) {
            throw 'PeriodServiceBase is an abstract class. You cannot instantiate from it.'
        }
      }
      getAll() {
        throw 'getAll() is not implemented'
      }
      add() {
        throw 'add() is not implemented'
      }
      deleteByNumber() {
        throw 'deleteByNumber() is not implemented'
      }
      deleteByYear() {
        throw 'deleteByYear() is not implemented'
      }
}