import SkOrdLsServiceBase from './SckOrdLsServiceBase';

class SckOrdLsServiceFake extends SkOrdLsServiceBase {
  getAll() {
    const _this = this;

    return new Promise((res, rej) => {
      const SckOrdLs = [
        {
            OrderID: 10248, CustomerID: 'VINET', EmployeeID: 5, OrderDate: new Date(8364186e5),
            ShipName: 'Vins et alcools Chevalier', ShipCity: 'Reims', ShipAddress: '59 rue de l Abbaye',
            ShipRegion: 'CJ', ShipPostalCode: '51100', ShipCountry: 'Brazil', Freight: 32.38, Verified: !0
        },
        
        {
            OrderID: 10255, CustomerID: 'RICSU', EmployeeID: 9, OrderDate: new Date(8371098e5),
            ShipName: 'Richter Supermarkt', ShipCity: 'Genève', ShipAddress: 'Starenweg 5',
            ShipRegion: 'CJ', ShipPostalCode: '1204', ShipCountry: 'Switzerland', Freight: 148.33, Verified: !0
        }];
      let data = localStorage.getItem('SckOrdLs');
      if (data) {
        try {
          data = JSON.parse(data);
        } catch (error) {

        }
      }
      if (Array.isArray(data)) {
        res(data)
      } else {
        _this.save(SckOrdLs);

        res(SckOrdLs);
      }
    });
  }
  save(data) {
    localStorage.setItem('SckOrdLs', JSON.stringify(data));
  }
}

export default SckOrdLsServiceFake;